package com.example.demo.dto;

import java.util.List;

public class ErrorDto {
    private List lineIds;
    private List columnIds;
    private List areaIds;

    public ErrorDto() {
    }

    public ErrorDto(List aLineIds, List aColumnIds, List aAreaIds) {
        this.lineIds = aLineIds;
        this.columnIds = aColumnIds;
        this.areaIds = aAreaIds;
    }

    public List getLineIds() {
        return lineIds;
    }

    public void setLineIds(List aLineIds) {
        this.lineIds = aLineIds;
    }

    public List<Integer> getColumnIds() {
        return columnIds;
    }

    public void setColumnIds(List aColumnIds) {
        this.columnIds = aColumnIds;
    }

    public List getAreaIds() {
        return areaIds;
    }

    public void setAreaIds(List aAreaIds) {
        this.areaIds = aAreaIds;
    }
}

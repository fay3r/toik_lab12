package com.example.demo.dto;

import lombok.NoArgsConstructor;
import java.util.List;

@NoArgsConstructor
public class BoardDto {
    private List<List> board;

    public BoardDto(List<List> aBoard) {
        this.board = aBoard;
    }

    public List<List> getBoard(){
        return board;
    }

    public void setBoard(List<List> aBoard) {
        this.board = aBoard;
    }

    @Override
    public String toString() {
        return "BoardDto{" +
                "board=" + board +
                '}';
    }
}

package com.example.demo.controller;


import com.example.demo.dto.ErrorDto;
import com.example.demo.service.SudokuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/sudoku")
public class SudokuApiController {

    @Autowired
    SudokuService sudokuService;
    private ErrorDto errorDto;
    @PostMapping(value = "/verify")
    public ResponseEntity verify(){
        errorDto = sudokuService.verifySudoku();
       return  errorDto == null ?  new ResponseEntity(HttpStatus.OK) : new ResponseEntity(errorDto,HttpStatus.BAD_REQUEST);
    }
}

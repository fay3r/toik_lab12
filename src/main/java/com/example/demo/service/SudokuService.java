package com.example.demo.service;

import com.example.demo.dto.ErrorDto;

public interface SudokuService {
    ErrorDto verifySudoku();
}

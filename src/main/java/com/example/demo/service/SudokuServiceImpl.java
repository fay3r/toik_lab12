package com.example.demo.service;

import com.example.demo.component.BoardComponent;
import com.example.demo.dto.ErrorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SudokuServiceImpl implements SudokuService {
    @Autowired
    BoardComponent boardComponent;

    //zrodlo https://stackoverflow.com/questions/34076389/java-sudoku-solution-verifier
    @Override
    public ErrorDto verifySudoku(){
        boardComponent.getCsv();
        System.out.println(boardComponent.getBoard().toString());
        List lineIds = new ArrayList<>();
        List columnIds = new ArrayList<>();
        List areaIds = new ArrayList<>();

        for(int row = 0; row < 9; row++)
            for(int col = 0; col < 8; col++)
                for(int col2 = col + 1; col2 < 9; col2++) {
                    if (boardComponent.getBoard().getBoard().get(row).get(col).equals(boardComponent.getBoard().getBoard().get(row).get(col2))) {
                        lineIds.add(row + 1);
                    }
                }

        for(int col = 0; col < 9; col++)
            for(int row = 0; row < 8; row++)
                for(int row2 = row + 1; row2 < 9; row2++)
                    if(boardComponent.getBoard().getBoard().get(row).get(col).equals(boardComponent.getBoard().getBoard().get(row2).get(col)))
                        columnIds.add(col+1);

        for(int row = 0; row < 9; row += 3)
            for(int col = 0; col < 9; col += 3)
                for(int pos = 0; pos < 8; pos++)
                    for(int pos2 = pos + 1; pos2 < 9; pos2++)
                        if(boardComponent.getBoard().getBoard().get(row + pos%3).get(col + pos/3).equals(boardComponent.getBoard().getBoard().get(row + pos2%3).get(col + pos2/3))) {
                            switch (row){
                                case 0:
                                    switch(col){
                                        case 0:
                                            areaIds.add(1);
                                            break;
                                        case 3:
                                            areaIds.add(2);
                                            break;
                                        case 6:
                                            areaIds.add(3);
                                            break;
                                    }
                                    break;
                                case 3:
                                    switch(col){
                                        case 0:
                                            areaIds.add(4);
                                            break;
                                        case 3:
                                            areaIds.add(5);
                                            break;
                                        case 6:
                                            areaIds.add(6);
                                            break;
                                    }
                                    break;
                                case 6:
                                    switch(col){
                                        case 0:
                                            areaIds.add(7);
                                            break;
                                        case 3:
                                            areaIds.add(8);
                                            break;
                                        case 6:
                                            areaIds.add(9);
                                            break;
                                    }

                                    break;
                            }
                        }

        if(!lineIds.isEmpty() || !columnIds.isEmpty() || !areaIds.isEmpty()){
            return new ErrorDto(lineIds,columnIds,areaIds);

        }
        return null;
    }
}


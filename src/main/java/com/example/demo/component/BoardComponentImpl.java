package com.example.demo.component;

import com.example.demo.dto.BoardDto;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class BoardComponentImpl implements BoardComponent {
    private static final String FILEPATH = "sudoku.csv";
    private BoardDto board;

    public BoardComponentImpl() {
        getCsv();
    }

    @Override
    public void getCsv() {
        List<List<String>> boardAsString = new ArrayList<>();
        try (CSVReader csvReader = new CSVReader(new FileReader(FILEPATH))) {
            String[] digit = null;
            while ((digit = csvReader.readNext()) != null) {
                boardAsString.add(Arrays.asList(digit));
            }
        } catch (CsvValidationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<List> boardAsInts = new ArrayList<>();
        for(List<String> lineOfStrings : boardAsString){
            List<Integer> lineOfInts = new ArrayList<>();
            for(String digit:lineOfStrings){
                lineOfInts.add(Integer.parseInt(digit));
            }
            boardAsInts.add(lineOfInts);
        }
        board = new BoardDto(boardAsInts);
        System.out.println(board.toString());
    }


    @Override
    public BoardDto getBoard() {
        return board;
    }
}

package com.example.demo.component;

import com.example.demo.dto.BoardDto;

public interface BoardComponent {
    void getCsv();
    BoardDto getBoard();
}
